package main

import (
	"fmt"
	"net/http"
	"os"

	"github.com/vladimirigorevitch/sproot/views"
)

func main() {
	err := views.PopulateTemplates()
	if err != nil {
		fmt.Println("Error views.PopulateTemplates: ", err.Error())
		os.Exit(1)
	}

	fs := http.FileServer(http.Dir("static"))
	http.Handle("/static/", http.StripPrefix("/static/", fs))
	http.Handle("/owls/", fs)

	http.HandleFunc("/main/", views.MainPage)
	http.HandleFunc("/signup/", views.SignUpFunc)
	http.HandleFunc("/signin/", views.LoginFunc)
	http.HandleFunc("/signout/", views.LogoutFunc)
	http.HandleFunc("/toRu/", views.ToRu)
	http.HandleFunc("/toEn/", views.ToEn)
	http.HandleFunc("/djangoapp/", views.ToDjangoapp)
	http.HandleFunc("/", views.HomePage)

	//get the port and run the server
	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}
	fmt.Println("running on port: ", port)
	err = http.ListenAndServe(":"+port, nil)
	if err != nil {
		fmt.Printf("error ListenAndServe: %q\n", err)
	}
}
