package utils

import (
	"fmt"
	"reflect"

	"golang.org/x/crypto/bcrypt"
)

// EncryptPassword ...
func EncryptPassword(pass string) (string, error) {
	if pass == "" {
		err := fmt.Errorf("util.EncryptPassword: empty parameters's been passed")
		return "", err
	}

	password := []byte(pass)
	hash, err := bcrypt.GenerateFromPassword(password, bcrypt.DefaultCost)
	if err != nil {
		return "", err
	}
	fmt.Println("hash: ", string(hash))
	fmt.Println("type: ", reflect.TypeOf(hash))
	return string(hash), nil
}

// ComparePassword key value from database and uname, pass from request/sess
func ComparePassword(pass, passFromDB string) (bool, error) {
	if passFromDB == "" || pass == "" {
		err := fmt.Errorf("util.ComparePassword: empty parameters's been passed")
		return false, err
	}

	res := bcrypt.CompareHashAndPassword([]byte(passFromDB), []byte(pass))
	if res != nil {
		return false, nil
	}
	return true, nil
}
