package utils

import (
	"crypto/md5"
	"fmt"
	"io"
	"io/ioutil"
	"regexp"
	"strconv"
	"strings"
	"time"
)

const (
	loginPassError = "The login and password's first character must be a letter, it must contain at least 4 characters and no more than 15 characters and no characters other than letters, numbers and the underscore may be used"
)

var validLoginPass = regexp.MustCompile(`^[a-zA-Z]\w{3,14}$`)

//UserInfoSanitization input user information sanitization and assertion
func UserInfoSanitization(username, password string) (string, string, error) {
	username = strings.TrimSpace(username)
	password = strings.TrimSpace(password)

	if ok := validLoginPass.MatchString(username); !ok {
		err := fmt.Errorf(loginPassError)
		return "", "", err
	}

	if ok := validLoginPass.MatchString(password); !ok {
		err := fmt.Errorf(loginPassError)
		return "", "", err
	}

	return username, password, nil
}

//GetNewToken generate unique form session-token
func GetNewToken() string {
	crutime := time.Now().Unix()
	h := md5.New()
	io.WriteString(h, strconv.FormatInt(crutime, 10))
	token := fmt.Sprintf("%x", h.Sum(nil))

	return token
}

// PutFilesToList add files names from a dir to a filesList
func PutFilesToList(dir string, filesList []string) ([]string, error) {
	var allFiles []string
	allFiles = filesList

	files, err := ioutil.ReadDir(dir)
	if err != nil {
		return nil, err
	}

	for _, file := range files {
		filename := file.Name()
		if strings.HasSuffix(filename, ".html") {
			allFiles = append(allFiles, dir+filename)
		}
	}
	return allFiles, nil
}

// SliceContainsInt true if s contains e
func SliceContainsInt(s []int, e int) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}

// SliceContainsString true if s contains e
func SliceContainsString(s []string, e string) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}
