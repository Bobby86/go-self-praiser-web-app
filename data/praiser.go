// this script manage message data for main page

package data

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"math/rand"
	"os"
	"strings"
	"time"

	"github.com/vladimirigorevitch/sproot/utils"
)

const (
	goodWordsFirstFilename  = "goodFirst"
	goodWordsSecondFilename = "goodSecond"
	badWordsFirstFilename   = "badFirst"  // filename for words for 1st position
	badWordSecondFilename   = "badSecond" // example: "you are <1st position> <2nd position>"
)

var (
	random *rand.Rand
	// contentMap   = make(map[string][]string) //TODO
	wordsMap     = make(map[string]map[string][]string) //[wordtype1] : {[locale1] : {"word1", "word2", ...}, [locale2]...}
	blancMessage = map[string]string{
		"en": "Let's try!", // blank message for main page on get request
		"ru": "Попробуй!",
	}
	praisePrefix = map[string]string{
		"en": "You are ",
		"ru": "Ты ",
	}
)

func init() {
	seed := rand.NewSource(time.Now().UnixNano())
	random = rand.New(seed)
	// read files and split lines
	filesDir := "./data/files/"
	files, err := ioutil.ReadDir(filesDir)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	for _, file := range files {
		filename := file.Name()

		if strings.HasPrefix(filename, goodWordsFirstFilename) {
			// put array of bad words for first message position // same for next conditions
			readFileLines(filesDir+filename, goodWordsFirstFilename)
		} else if strings.HasPrefix(filename, goodWordsSecondFilename) {
			readFileLines(filesDir+filename, goodWordsSecondFilename)
		} else if strings.HasPrefix(filename, badWordsFirstFilename) {
			readFileLines(filesDir+filename, badWordsFirstFilename)
		} else if strings.HasPrefix(filename, badWordSecondFilename) {
			readFileLines(filesDir+filename, badWordSecondFilename)
		}
	}

	// printWordsMap()
}

func readFileLines(filePath, contentKey string) bool {
	var langKey string

	// AcceptableLanguages variable are from the same package (data) defined in data.go file
	for _, val := range AcceptableLanguages {
		if strings.HasSuffix(filePath, val) {
			langKey = val
			break
		}
	}
	if langKey == "" {
		fmt.Println("readFileLines: language not found (check filenames *_<language>)")
		return false
	}

	file, err := os.OpenFile(filePath, os.O_RDONLY, 0666)
	if err != nil {
		fmt.Println("readFileLines: ", err)
		return false
	}

	defer file.Close()
	var result []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		// fmt.Println(scanner.Text())
		word := strings.TrimSpace(scanner.Text())
		if word == "" {
			continue
		}
		result = append(result, word)
	}
	if err = scanner.Err(); err != nil {
		fmt.Println(err)
	}

	if len(result) < 1 {
		fmt.Println("readFileLines: no lines has been read")
		err = fmt.Errorf("no lines has been read")
		return false
	}

	// contentMap[contentKey] = result

	// if len(wordsMap[contentKey]) == 0 {
	localizedMap, ok := wordsMap[contentKey]
	if !ok {
		localizedMap = make(map[string][]string)
		wordsMap[contentKey] = localizedMap
	}
	localizedMap[langKey] = result

	// printContentMap(wordsMap[contentKey])

	return true
}

func getRandomWord(wordType int, langKey string) (string, error) {
	var result string
	var err error

	switch wordType {
	// good word for 1st position
	case 0:
		result, err = extractRandomWord(goodWordsFirstFilename, langKey)
	// good word for 2nd position
	case 1:
		result, err = extractRandomWord(goodWordsSecondFilename, langKey)
	// bad word for 1st position
	case 2:
		result, err = extractRandomWord(badWordsFirstFilename, langKey)
	// bad word for 2nd position
	case 3:
		result, err = extractRandomWord(badWordSecondFilename, langKey)
	default:
		err = fmt.Errorf("Unknown word type %v\n", wordType)
		result = ""
	}
	return result, err
}

func extractRandomWord(key, langKey string) (string, error) {
	var err error

	if array, ok := wordsMap[key][langKey]; ok {
		if length := len(array); length > 0 {
			return array[random.Intn(length)], nil
		} //else
		err = fmt.Errorf("ExtractRandomWord: list is empty, key: %q\n", key)
	} else {
		err = fmt.Errorf("ExtractRandomWord: the key is not valid, key: %q\n", key)
	}

	return "", err
}

// GetBlankMessage age question string
func GetBlankMessage(lang string) string {

	message, ok := blancMessage[lang]
	if ok {
		return message
	}

	return blancMessage["en"]
}

// GetNewMessage words types: 0 - good word 1st position, 1 - good word 2nd position, 2 - bad word 1st ... etc.
func GetNewMessage(wordType1, wordType2 int, langKey string) string {
	var first, second, result string

	// AcceptableLanguages variable are from the same package (data) defined in data.go file
	if ok := utils.SliceContainsString(AcceptableLanguages, langKey); !ok {
		langKey = "en"
	}

	first, err := getRandomWord(wordType1, langKey)
	if err != nil || first == "" {
		fmt.Println(err)
		first = "wonderful"
	}

	second, err = getRandomWord(wordType2, langKey)
	if err != nil || second == "" {
		fmt.Println(err)
		second = "being"
	}

	// result = "You are " + first + " " + second + "!"

	for strings.Contains(first, second) || strings.Contains(second, first) {
		second, err = getRandomWord(wordType2, langKey)
		if err != nil || second == "" {
			fmt.Println(err)
			second = "being"
		}
		// result = "You are double " + first + " " + second + "!"
	}
	result = praisePrefix[langKey] + first + " " + second + "!"
	// fmt.Println(first + " " + second)
	return result
}

// [min, max)
func getRandomIndexes(min, max int) (int, int) {
	tmp := max - min

	if min < 0 || max < 0 || tmp < 1 {
		return 0, 1
	}

	i1 := rand.Intn(max-min) + min
	i2 := rand.Intn(max-min) + min

	for i1 == i2 {
		i2 = rand.Intn(max-min) + min
	}

	return i1, i2
}

func printWordsMap() {
	fmt.Println("#############################################")
	for key, val := range wordsMap {
		fmt.Println(key)
		for k, v := range val {
			fmt.Println("----", len(v), " ", k, ": ", v)
		}
	}
}

func printContentMap(m map[string][]string) {
	fmt.Println("#############################################")
	for k, v := range m {
		fmt.Println("#### ", len(v), " ", k, ": ", v)
	}
}
