package data

const (
	// DefaultLanguage declare in localization package
	DefaultLanguage = "en"
)

// AcceptableLanguages are used for sanitize input
var AcceptableLanguages = []string{"en", "ru"}

// TemplatesDirectories html files directories
var TemplatesDirectories = []string{"./templates/_nested/", "./templates/en/", "./templates/ru/"}
