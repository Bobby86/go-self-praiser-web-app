package views

import (
	"fmt"
	"html/template"
	"net/http"
	"strconv"

	"github.com/vladimirigorevitch/sproot/data"
	"github.com/vladimirigorevitch/sproot/sessions"
	"github.com/vladimirigorevitch/sproot/utils"
)

// Message get/set values from the form
type Message struct {
	//variable of the age confirmation (true if 18+)
	Eighteen bool
	//true if praise word choosen for first word in the massage (by radiobutton)	//swearword otherwise
	PraiseFirstChecked bool
	//true if praise word choosen for second word in the massage (by radiobutton)
	PraiseSecondChecked bool
	//generated message ("You are <first word> <second word>!")
	PraiseMessage string
	//language of message
	Lang string
	//user login to display if he's signed in
	Username string
}

var (
	eighteen bool // age question // if false bad words not available
	// en
	homeTemplateEn   *template.Template
	mainTemplateEn   *template.Template
	loginTemplateEn  *template.Template
	signupTemplateEn *template.Template
	// ru
	homeTemplateRu   *template.Template
	mainTemplateRu   *template.Template
	loginTemplateRu  *template.Template
	signupTemplateRu *template.Template
)

// PopulateTemplates read and parse templates
func PopulateTemplates() error {
	var allFiles []string
	for _, dir := range data.TemplatesDirectories {
		files, err := utils.PutFilesToList(dir, allFiles)
		if err != nil {
			return err
		}
		allFiles = files
	}

	templates, err := template.ParseFiles(allFiles...)
	if err != nil {
		return err
	}
	// en
	homeTemplateEn = templates.Lookup("home.html")
	mainTemplateEn = templates.Lookup("main.html")
	loginTemplateEn = templates.Lookup("signin.html")
	signupTemplateEn = templates.Lookup("signup.html")
	// ru
	homeTemplateRu = templates.Lookup("home_ru.html")
	mainTemplateRu = templates.Lookup("main_ru.html")
	loginTemplateRu = templates.Lookup("signin_ru.html")
	signupTemplateRu = templates.Lookup("signup_ru.html")

	return nil
}

// HomePage "/"
func HomePage(w http.ResponseWriter, r *http.Request) {
	var err error

	if r.Method == "POST" {
		err = r.ParseForm()
		if err != nil {
			fmt.Println("HomePage 1: ", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		// get the value from the age confirmation buttons
		if r.PostFormValue("yes") == "yes" {
			eighteen = true
		} else {
			eighteen = false
		}

		http.Redirect(w, r, "/main/", 302)
		return
	}
	// GET request
	lang := sessions.GetSessionLanguage(r)  // choosen or default language (default if error anyway)
	uname := sessions.GetCurrentUserName(r) // empty if logged off
	message := Message{Lang: lang, Username: uname}

	switch lang {
	case "en":
		err = homeTemplateEn.Execute(w, message)
	case "ru":
		err = homeTemplateRu.Execute(w, message)
	default:
		http.Redirect(w, r, "/", 302)
		return
	}
	if err != nil {
		fmt.Println("HomePage template execution error: ", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

}

// MainPage "/main/"
func MainPage(w http.ResponseWriter, r *http.Request) {
	var word1Type, word2Type int // words types
	var err error
	var message Message
	lang := sessions.GetSessionLanguage(r)  // choosen or default language (default if error anyway)
	uname := sessions.GetCurrentUserName(r) // empty if logged off

	if r.Method != "POST" {
		// message := Message{Eighteen: eighteen, Lang: lang, Username: uname}
		// message.PraiseFirstChecked = true
		// message.PraiseSecondChecked = true
		blankMessage := data.GetBlankMessage(lang)
		message = setMessage(eighteen, 0, 1, blankMessage, lang, uname)
	} else {
		var msg string
		// age filter
		if eighteen {
			//0/2 - praising/swearing word for 1st position, 1/3 - for 2nd
			word1Type, word2Type = parseWordsTypes(r)
			msg = data.GetNewMessage(word1Type, word2Type, lang) //return anything anyway
		} else {
			word1Type, word2Type = 0, 1
			msg = data.GetNewMessage(word1Type, word2Type, lang) //age < 18 : only praise + praise
		}
		message = setMessage(eighteen, word1Type, word2Type, msg, lang, uname)
	}

	switch lang {
	case "en":
		err = mainTemplateEn.Execute(w, message)
	case "ru":
		err = mainTemplateRu.Execute(w, message)
	default:
		http.Redirect(w, r, "/", 302)
		return
	}
	if err != nil {
		fmt.Println("MainPage template execution error: ", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func parseWordsTypes(r *http.Request) (int, int) {
	// parse radio buttons values to determine choosen types of the words
	err := r.ParseForm()
	if err == nil {
		type1, err := strconv.Atoi(r.Form.Get("radio1"))
		if err != nil {
			fmt.Println("parseWordsTypes failure: ", err)
			type1 = 0
		}
		type2, err := strconv.Atoi(r.Form.Get("radio2"))
		if err != nil {
			fmt.Println("parseWordsTypes failure: ", err)
			type2 = 1
		}
		// fmt.Println("type1 = ", type1, "; type2 = ", type2)
		if ok := utils.SliceContainsInt([]int{0, 2}, type1); !ok {
			type1 = 0
		}
		if ok := utils.SliceContainsInt([]int{1, 3}, type2); !ok {
			type2 = 1
		}
		return type1, type2
	}
	fmt.Println("parseWordsTypes failure: ", err)
	return 0, 1 //praising words types
}

func setMessage(age bool, wtype1, wtype2 int, message, language, uname string) Message {
	var result Message

	if age { // age >= 18
		// set radio buttons state
		if wtype1 == 0 {
			result.PraiseFirstChecked = true
		} else {
			result.PraiseFirstChecked = false
		}

		if wtype2 == 1 {
			result.PraiseSecondChecked = true
		} else {
			result.PraiseSecondChecked = false
		}
	}

	result.Eighteen = age
	result.PraiseMessage = message
	result.Lang = language
	result.Username = uname

	return result
}

// localizators	//TORefactoring

// ToRu switch content language to russian
func ToRu(w http.ResponseWriter, r *http.Request) {
	err := sessions.SetSessionLanguage(w, r, "ru")
	if err != nil {
		// default language will be set
		fmt.Println("SetSessionLanguage:sessions failure: ", err)
	}

	http.Redirect(w, r, "/", 302)
}

// ToEn switch content language to english
func ToEn(w http.ResponseWriter, r *http.Request) {
	err := sessions.SetSessionLanguage(w, r, "en")
	if err != nil {
		// default language will be set
		fmt.Println("SetSessionLanguage:sessions failure: ", err)
	}

	http.Redirect(w, r, "/", 302)
}

// ToDjangoapp redirect to django website
func ToDjangoapp(w http.ResponseWriter, r *http.Request) {
	http.Redirect(w, r, "https://blank-demo-djangoapp.herokuapp.com/", 301)
}
