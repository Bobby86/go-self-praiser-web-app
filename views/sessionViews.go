package views

import (
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/vladimirigorevitch/sproot/data"
	"github.com/vladimirigorevitch/sproot/database"
	"github.com/vladimirigorevitch/sproot/sessions"
	"github.com/vladimirigorevitch/sproot/utils"
)

type LoginMessages struct {
	ErrorMessage string
	ShowLink     string // #showsignin || #showsignup
	Token        string
	Lang         string
}

// ------ authentication part ----

//LoginFunc add a cookie to cookie store for managing authentication
func LoginFunc(w http.ResponseWriter, r *http.Request) {
	session, err := sessions.Store.Get(r, "session")
	if err != nil {
		log.Println("error identifying session")
		message := LoginMessages{ShowLink: "#showsignup", Lang: data.DefaultLanguage} //TODO get language from previous
		err = executeTemplate(w, message, "signin")
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
		return
	}
	lang := sessions.GetSessionLanguage(r)

	switch r.Method {
	case "GET":
		// create token, save it to the empty session, put it in a form
		token := utils.GetNewToken()
		session.Values["token"] = token
		session.Save(r, w)
		message := LoginMessages{ShowLink: "#showsignup", Token: token, Lang: lang}
		err = executeTemplate(w, message, "signin")
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
	case "POST":
		log.Print("Login: inside POST")
		r.ParseForm()
		username := r.Form.Get("username")
		password := r.Form.Get("password")
		var message = LoginMessages{ShowLink: "#showsignup", Lang: lang}
		// validate token
		token := r.Form.Get("token")
		if token == "" || token != session.Values["token"] {
			// possible attack
			return
		} // else
		message.Token = token

		// sanitize form info
		username, password, err := utils.UserInfoSanitization(username, password)
		if err != nil {
			message.ErrorMessage = err.Error()
			err = executeTemplate(w, message, "signin")
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			return
		}
		// check user validity
		valid, err := database.ValidateUser(username, password)
		if err == nil {
			if valid {
				session.Values["loggedin"] = "true"
				session.Values["username"] = username
				err := session.Save(r, w)
				if err != nil {
					fmt.Println("Error saving session: ", err)
				}
				log.Print("user ", username, " authenticated")
				http.Redirect(w, r, "/", 302)
				return
			} // else
			message.ErrorMessage = "user name or password not valid"
			log.Print("User is not valid " + username)
		} else {
			message.ErrorMessage = err.Error()
		}
		err = executeTemplate(w, message, "signin")
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
	default:
		http.Redirect(w, r, "/signin/", http.StatusUnauthorized)
	}
}

//LogoutFunc delete sess info from cookie store (Store)
func LogoutFunc(w http.ResponseWriter, r *http.Request) {
	session, err := sessions.Store.Get(r, "session")
	if err == nil {
		if session.Values["loggedin"] != "false" {
			session.Values["loggedin"] = "false"
			session.Values["username"] = ""
			session.Values["token"] = ""
			// session.Values["userLanguage"] = ""
			err = session.Save(r, w)
		}
	}
	http.Redirect(w, r, "/", 302)
}

//--- registration part -----

//SignUpFunc validate and register new user info to a database
func SignUpFunc(w http.ResponseWriter, r *http.Request) {
	session, err := sessions.Store.Get(r, "session")
	if err != nil {
		message := LoginMessages{ErrorMessage: "Error identifying session", ShowLink: "#showsignin", Lang: data.DefaultLanguage} //TODO get language from previous
		err := executeTemplate(w, message, "signup")
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
		return
	}
	lang := sessions.GetSessionLanguage(r)

	switch r.Method {
	case "GET":
		token := utils.GetNewToken()
		session.Values["token"] = token
		session.Save(r, w)
		message := LoginMessages{ShowLink: "#showsignin", Token: token, Lang: lang, ErrorMessage: "Registration"} //TOFIX registration by the language
		err := executeTemplate(w, message, "signup")
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
	case "POST":
		var message = LoginMessages{ShowLink: "#showsignin", Lang: lang} // struct with error message and token
		r.ParseForm()
		username := r.Form.Get("username")
		password := r.Form.Get("password")
		//sex := r.Form.Fet("gender")

		// token validation
		token := r.Form.Get("token")
		if token == "" || token != session.Values["token"] {
			// possible attack
			return
		}
		message.Token = token // else - reuse

		// sanitize form info
		username, password, err := utils.UserInfoSanitization(username, password)
		if err != nil {
			message.ErrorMessage = err.Error()
			err = executeTemplate(w, message, "signin")
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			return
		}

		// save new user // or throw an error if user already exist
		res, err := database.AddUser(username, password, time.Time{}) //TODO birthdate stuff
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		} else if res != "" {
			message.ErrorMessage = res
			fmt.Println(message.ErrorMessage)
			err = executeTemplate(w, message, "signup")
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
		} else {
			http.Redirect(w, r, "/signin/", 302)
		}
	default:
		http.Redirect(w, r, "/", http.StatusBadRequest)
	}
}

// ---- helpers ----

func executeTemplate(w http.ResponseWriter, message LoginMessages, tmplType string) error {
	var err error
	switch message.Lang {
	case "ru":
		if tmplType == "signin" {
			err = loginTemplateRu.Execute(w, message)
		} else { // signup
			err = signupTemplateRu.Execute(w, message)
		}
	default: // case "en"
		if tmplType == "signin" {
			err = loginTemplateEn.Execute(w, message)
		} else { // signup
			err = signupTemplateEn.Execute(w, message)
		}
	}

	return err
}
