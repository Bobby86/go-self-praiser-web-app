Demonstration Go web application, author and developer Rybnikov Vladimir

Deployed version: self-praiser-app.herokuapp.com

Contains:

- dynamic and static content

- multi-language support  

- client sessions

- client web includes html5, css3, JS/JQuery