package database

import (
	"database/sql"
	"fmt"
	"os"
	"time"

	_ "github.com/lib/pq"
	"github.com/vladimirigorevitch/sproot/utils"
)

var (
	db *sql.DB
)

func init() {
	var err error
	// db, err = sql.Open("postgres", "postgres://postgres:pass@localhost/users?sslmode=disable")
	db, err = sql.Open("postgres", os.Getenv("DATABASE_URL"))
	if err != nil {
		fmt.Println("Cannot open database: ", err)
		return
	}

	db.SetMaxIdleConns(100)
	const statement = `CREATE TABLE IF NOT EXISTS users (
        id SERIAL PRIMARY KEY,
        username VARCHAR (20) NOT NULL,
        password VARCHAR (100) NOT NULL,
        birthdate DATE DEFAULT NULL,
        UNIQUE (id, username)) WITH (OIDS=FALSE);`

	_, err = db.Exec(statement)
	if err != nil {
		fmt.Println("Cannot create table \"users\": ", err)
	}

	AddUser("Admin", "Test", time.Time{})
}

// ValidateUser true if user was found in the database
func ValidateUser(uname, pass string) (bool, error) {
	var passFromDb string
	fmt.Println("validating user: ", uname)
	db.QueryRow("SELECT password FROM users WHERE username=$1", uname).Scan(&passFromDb)
	fmt.Println("passDB: ", passFromDb)
	fmt.Println("pass__: ", pass)
	if passFromDb == "" {
		err := fmt.Errorf("user login or password not found")
		return false, err
	}

	match, err := utils.ComparePassword(pass, passFromDb)
	if err != nil {
		fmt.Println("got error comparing passwords: ", err)
		err = fmt.Errorf("user login or password not found")
		return false, err
	} else if match {
		fmt.Println("user are valid")
		return true, nil
	}

	err = fmt.Errorf("user login or password not found")
	return false, err
}

//AddUser insert a new unique user to the database
func AddUser(uname, pass string, birthdate time.Time) (string, error) {
	fmt.Println("Add user called")
	exists, err := checkLoginInDB(uname)
	if err != nil {
		return "", err
	}
	if exists {
		return "username " + uname + " already exist", nil
	}
	// login hasn't been found in the database
	//prepare statement
	statement, err := db.Prepare("INSERT INTO users(username,password,birthdate) VALUES($1,$2,$3)")
	if err != nil {
		fmt.Println("AddUser prepare failure: ", err)
		return "", err
	}
	// encrypt password
	pass, err = utils.EncryptPassword(pass)
	if err != nil {
		fmt.Println("AddUser failure: password encryption falure ", err)
		return "", err
	}
	//execute statement
	_, err = statement.Exec(uname, pass, birthdate)
	if err != nil {
		fmt.Println("AddUser exec failure: ", err)
		return "", err
	}
	fmt.Println("User ", uname, " successfully added")
	return "", nil
}

// check if login is unique in a database
func checkLoginInDB(login string) (bool, error) {
	// query
	var dbLogin string
	db.QueryRow("SELECT username FROM users WHERE username=$1", login).Scan(&dbLogin)
	fmt.Println("login from db: ", dbLogin)
	if dbLogin == login {
		return true, nil //same login exists in the database
	}

	return false, nil //lpgin not found in the database
}
