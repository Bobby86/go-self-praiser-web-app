package sessions

import (
	"fmt"
	"net/http"

	"github.com/gorilla/sessions"

	"github.com/vladimirigorevitch/sproot/data"
	"github.com/vladimirigorevitch/sproot/utils"
)

// Store is cookie session data storage
var Store = sessions.NewCookieStore([]byte("super-duper-secret-password"))

// GetSessionLanguage returns default or language("en"etc) of user choice
func GetSessionLanguage(r *http.Request) string {
	sess, err := Store.Get(r, "session")
	if err != nil {
		fmt.Println("GetSessionLanguage:sessions failure: ", err)
		return data.DefaultLanguage
	} //else

	lang, ok := sess.Values["userLanguage"].(string)
	if ok {
		return lang
	} //else
	fmt.Println("GetSessionLanguage: userLanguage value of unexpected type; ", lang)

	return data.DefaultLanguage
}

// SetSessionLanguage set passed language(newLanguage) as session content language
func SetSessionLanguage(w http.ResponseWriter, r *http.Request, newLanguage string) error {
	sess, err := Store.Get(r, "session")
	if err != nil {
		return err
	}

	if currentLanguage, ok := sess.Values["userLanguage"]; ok {
		if currentLanguage == newLanguage {
			return nil // same language value passed, just return
		}
	} // else
	if ok := utils.SliceContainsString(data.AcceptableLanguages, newLanguage); ok {
		sess.Values["userLanguage"] = newLanguage
		sess.Save(r, w)
		return nil
	} // else
	// if newLanguage do not pass sanitization set it as default language
	fmt.Println("SetSessionLanguage:corupted data detected: ", newLanguage)
	sess.Values["userLanguage"] = data.DefaultLanguage
	sess.Save(r, w)
	return nil
}

// IsLoggedIn ...
func IsLoggedIn(r *http.Request) bool {
	session, err := Store.Get(r, "session")
	if err == nil && (session.Values["loggedin"] == "true") {
		return true
	}
	return false
}

// GetCurrentUserName ...
func GetCurrentUserName(r *http.Request) string {
	session, err := Store.Get(r, "session")
	if err != nil {
		return ""
	}

	uname, ok := session.Values["username"].(string)
	if ok {
		return uname
	}
	fmt.Println("GetCurrentUserName: username value of unexpected type; ", uname)
	return ""
}
